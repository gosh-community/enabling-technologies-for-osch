---
layout: tech
title: 3D printer
fields:
  - tools
---

3D printers revolutionised open hardware production. One of the most famous open hardware projects is the RepRap project to make self replicating 3D printers. This has lead to a number of open source, low cost 3D printers including printers by:

* [RepRap](https://reprapltd.com/product-category/3d-printers/)
* [Prusa](https://www.prusa3d.com/)
* [Ultimaker](https://ultimaker.com/)
