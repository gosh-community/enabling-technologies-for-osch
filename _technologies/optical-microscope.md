---
layout: tech
title: Optical Microscope
fields:
  - biology
  - physics
---

Microscopes are essential for many fields of scientific research. Optical microscopes can range in cost significantly. There are numerous open source microscope projects including

* [OpenFlexure Microscope](https://openflexure.org/projects/microscope/)
* [FlyPi](https://open-labware.net/projects/flypi/)
* [Public Lab community microscope](https://publiclab.org/wiki/micro)
* [OpenSPIM](https://openspim.org)