---
layout: field
title: Tools for manufacturing OScH
field_id: tools
---

The tools that are required to build open science hardware.
