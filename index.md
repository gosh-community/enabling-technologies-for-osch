---
layout: home
---

<div class="flex-container">
    <div class="flex-grow flex-container flex-container-vertical">
        {% for field in site.fields %}
            <div class="flex-container flex-auto flex-container-horizontal flex-valign-center elevated padding-small float-left margin-bottom-small">
                <div class="flex-grow">
                    <a href="{{ field.url | prepend: site.baseurl }}" class="field-title"><h2 >{{ field.title }}</h2></a>
                    <a href="{{ field.url | prepend: site.baseurl }}">{{ field.excerpt }}</a>
                </div>
                {% if field.icon %}
                    <div class="flex-auto margin-left-small">
                        <img src="{{ category.hero }}" alt="{{ category.title }}" class="image-small no-shadow">
                    </div>
                {% endif %}
            </div>
        {% endfor %}
    </div>
</div>

